%global pypi_name pytype

Name:		python-%{pypi_name}
Version:	2024.10.11
Release:	1
Summary: 	Python type inferencer

License:	Apache 2.0
URL:  		https://github.com/google/pytype
Source0:	https://files.pythonhosted.org/packages/9c/7a/6fd33673f9c7b9e5f4f8107028c323b1c72acc0f909f1b9b3391a31ea604/pytype-2024.10.11.tar.gz
Patch1:		0001-fix-parse-error-py.patch

%description
Pytype checks and infers types for your Python code - without requiring type annotations. 

%package -n python3-%{pypi_name}
Summary:	Python type inferencer
Provides:	python-%{pypi_name}

BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-pybind11
BuildRequires:  gcc-c++
BuildRequires:  ninja-build

%description -n python3-%{pypi_name}
Pytype checks and infers types for your Python code - without requiring type annotations. 

%prep
%autosetup -p1 -n %{pypi_name}-%{version}

%build
%pyproject_build

%install
%pyproject_install

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.md
%{python3_sitearch}/pytype*.dist-info/
%{python3_sitearch}/pytype/
%{python3_sitearch}/pytype_extensions/
%{python3_sitearch}/third_party/
%{_bindir}/*

%changelog
* Wed Nov 27 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 2024.10.11-1
- Update package with version 2024.10.11
  Add a comment about collections.abc vs typing
  Dropped support for Python 3.8 and 3.9
  Add pytype_extensions to CMakeLists_toplevel
  Add exhaustive match tracking for Literal values
  Add missing buffer methods to memoryview
  Add tracking bug for PEP 646 support
  Support type narrowing for match cases with as captures
  Do not treat await calls as loops in 3.11
  Adjust line numbers for reporting bad decorator calls
  Support pattern matching literal sequences
  Add exception handling for python 3.11

* Tue Apr 02 2024 zhangxianting <zhangxianting@uniontech.com> - 2023.9.19-2
- Increase the upper limit version of networkx to fix install, networkx<3.2 to networkx<=3.2

* Wed Sep 20 2023 mengzhaoa <mengzhaoa@isoftstone.com> - 2023.9.19-1
- Init package.
